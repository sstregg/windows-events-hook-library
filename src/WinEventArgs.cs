﻿using System;

namespace WindowsEventsHook
{
    /// <summary>
    /// 
    /// </summary>
    public class WinEventArgs
    {
        internal WinEventArgs ( ) { }
        /// <summary>
        /// Specifies the event that occurred.
        /// </summary>
        public EventHookType EventType { get; internal set; }
        /// <summary>
        /// Handle to the window that generates the event, or NULL if no window is associated with the event. 
        /// For example, the mouse pointer is not associated with a window.
        /// </summary>
        public IntPtr WindowHandle { get; internal set; }
        /// <summary>
        /// Identifies the object associated with the event.
        /// </summary>
        public int ObjectId { get; internal set; }
        /// <summary>
        /// Identifies whether the event was triggered by an object or a child element of the object.
        /// </summary>
        public int ChildId { get; internal set; }
        /// <summary>
        /// Identifies the thread that generated the event, or the thread that owns the current window.
        /// </summary>
        public uint EventThreadId { get; internal set; }
        /// <summary>
        /// Specifies the time, in milliseconds, that the event was generated.
        /// </summary>
        public uint EventTime { get; internal set; }
    }
}
