﻿namespace WindowsEventsHook
{
    #region ~ EVENT DEFINITION (from WinUser.h) ~
    ///*
    // * EVENT DEFINITION
    // */
    //#define EVENT_MIN           0x00000001
    //#define EVENT_MAX           0x7FFFFFFF

    ///*
    // *  EVENT_SYSTEM_SOUND
    // *  Sent when a sound is played.  Currently nothing is generating this, we
    // *  this event when a system sound (for menus, etc) is played.  Apps
    // *  generate this, if accessible, when a private sound is played.  For
    // *  example, if Mail plays a "New Mail" sound.
    // *
    // *  System Sounds:
    // *  (Generated by PlaySoundEvent in USER itself)
    // *      hwnd            is NULL
    // *      idObject        is OBJID_SOUND
    // *      idChild         is sound child ID if one
    // *  App Sounds:
    // *  (PlaySoundEvent won't generate notification; up to app)
    // *      hwnd + idObject gets interface pointer to Sound object
    // *      idChild identifies the sound in question
    // *  are going to be cleaning up the SOUNDSENTRY feature in the control panel
    // *  and will use this at that time.  Applications implementing WinEvents
    // *  are perfectly welcome to use it.  Clients of IAccessible* will simply
    // *  turn around and get back a non-visual object that describes the sound.
    // */
    //#define EVENT_SYSTEM_SOUND              0x0001

    ///*
    // * EVENT_SYSTEM_ALERT
    // * System Alerts:
    // * (Generated by MessageBox() calls for example)
    // *      hwnd            is hwndMessageBox
    // *      idObject        is OBJID_ALERT
    // * App Alerts:
    // * (Generated whenever)
    // *      hwnd+idObject gets interface pointer to Alert
    // */
    //#define EVENT_SYSTEM_ALERT              0x0002

    ///*
    // * EVENT_SYSTEM_FOREGROUND
    // * Sent when the foreground (active) window changes, even if it is changing
    // * to another window in the same thread as the previous one.
    // *      hwnd            is hwndNewForeground
    // *      idObject        is OBJID_WINDOW
    // *      idChild    is INDEXID_OBJECT
    // */
    //#define EVENT_SYSTEM_FOREGROUND         0x0003

    ///*
    // * Menu
    // *      hwnd            is window (top level window or popup menu window)
    // *      idObject        is ID of control (OBJID_MENU, OBJID_SYSMENU, OBJID_SELF for popup)
    // *      idChild         is CHILDID_SELF
    // *
    // * EVENT_SYSTEM_MENUSTART
    // * EVENT_SYSTEM_MENUEND
    // * For MENUSTART, hwnd+idObject+idChild refers to the control with the menu bar,
    // *  or the control bringing up the context menu.
    // *
    // * Sent when entering into and leaving from menu mode (system, app bar, and
    // * track popups).
    // */
    //#define EVENT_SYSTEM_MENUSTART          0x0004
    //#define EVENT_SYSTEM_MENUEND            0x0005

    ///*
    // * EVENT_SYSTEM_MENUPOPUPSTART
    // * EVENT_SYSTEM_MENUPOPUPEND
    // * Sent when a menu popup comes up and just before it is taken down.  Note
    // * that for a call to TrackPopupMenu(), a client will see EVENT_SYSTEM_MENUSTART
    // * followed almost immediately by EVENT_SYSTEM_MENUPOPUPSTART for the popup
    // * being shown.
    // *
    // * For MENUPOPUP, hwnd+idObject+idChild refers to the NEW popup coming up, not the
    // * parent item which is hierarchical.  You can get the parent menu/popup by
    // * asking for the accParent object.
    // */
    //#define EVENT_SYSTEM_MENUPOPUPSTART     0x0006
    //#define EVENT_SYSTEM_MENUPOPUPEND       0x0007


    ///*
    // * EVENT_SYSTEM_CAPTURESTART
    // * EVENT_SYSTEM_CAPTUREEND
    // * Sent when a window takes the capture and releases the capture.
    // */
    //#define EVENT_SYSTEM_CAPTURESTART       0x0008
    //#define EVENT_SYSTEM_CAPTUREEND         0x0009

    ///*
    // * Move Size
    // * EVENT_SYSTEM_MOVESIZESTART
    // * EVENT_SYSTEM_MOVESIZEEND
    // * Sent when a window enters and leaves move-size dragging mode.
    // */
    //#define EVENT_SYSTEM_MOVESIZESTART      0x000A
    //#define EVENT_SYSTEM_MOVESIZEEND        0x000B

    ///*
    // * Context Help
    // * EVENT_SYSTEM_CONTEXTHELPSTART
    // * EVENT_SYSTEM_CONTEXTHELPEND
    // * Sent when a window enters and leaves context sensitive help mode.
    // */
    //#define EVENT_SYSTEM_CONTEXTHELPSTART   0x000C
    //#define EVENT_SYSTEM_CONTEXTHELPEND     0x000D

    ///*
    // * Drag & Drop
    // * EVENT_SYSTEM_DRAGDROPSTART
    // * EVENT_SYSTEM_DRAGDROPEND
    // * Send the START notification just before going into drag&drop loop.  Send
    // * the END notification just after canceling out.
    // * Note that it is up to apps and OLE to generate this, since the system
    // * doesn't know.  Like EVENT_SYSTEM_SOUND, it will be a while before this
    // * is prevalent.
    // */
    //#define EVENT_SYSTEM_DRAGDROPSTART      0x000E
    //#define EVENT_SYSTEM_DRAGDROPEND        0x000F

    ///*
    // * Dialog
    // * Send the START notification right after the dialog is completely
    // *  initialized and visible.  Send the END right before the dialog
    // *  is hidden and goes away.
    // * EVENT_SYSTEM_DIALOGSTART
    // * EVENT_SYSTEM_DIALOGEND
    // */
    //#define EVENT_SYSTEM_DIALOGSTART        0x0010
    //#define EVENT_SYSTEM_DIALOGEND          0x0011

    ///*
    // * EVENT_SYSTEM_SCROLLING
    // * EVENT_SYSTEM_SCROLLINGSTART
    // * EVENT_SYSTEM_SCROLLINGEND
    // * Sent when beginning and ending the tracking of a scrollbar in a window,
    // * and also for scrollbar controls.
    // */
    //#define EVENT_SYSTEM_SCROLLINGSTART     0x0012
    //#define EVENT_SYSTEM_SCROLLINGEND       0x0013

    ///*
    // * Alt-Tab Window
    // * Send the START notification right after the switch window is initialized
    // * and visible.  Send the END right before it is hidden and goes away.
    // * EVENT_SYSTEM_SWITCHSTART
    // * EVENT_SYSTEM_SWITCHEND
    // */
    //#define EVENT_SYSTEM_SWITCHSTART        0x0014
    //#define EVENT_SYSTEM_SWITCHEND          0x0015

    ///*
    // * EVENT_SYSTEM_MINIMIZESTART
    // * EVENT_SYSTEM_MINIMIZEEND
    // * Sent when a window minimizes and just before it restores.
    // */
    //#define EVENT_SYSTEM_MINIMIZESTART      0x0016
    //#define EVENT_SYSTEM_MINIMIZEEND        0x0017


    //#if(_WIN32_WINNT >= 0x0600)
    //#define EVENT_SYSTEM_DESKTOPSWITCH      0x0020
    //#endif /* _WIN32_WINNT >= 0x0600 */


    //#if(_WIN32_WINNT >= 0x0602)
    //// AppGrabbed: HWND = hwnd of app thumbnail, objectID = 0, childID = 0
    //#define EVENT_SYSTEM_SWITCHER_APPGRABBED    0x0024
    //// OverTarget: HWND = hwnd of app thumbnail, objectID =
    ////            1 for center
    ////            2 for near snapped
    ////            3 for far snapped
    ////            4 for prune
    ////            childID = 0
    //#define EVENT_SYSTEM_SWITCHER_APPOVERTARGET 0x0025
    //// Dropped: HWND = hwnd of app thumbnail, objectID = <same as above>, childID = 0
    //#define EVENT_SYSTEM_SWITCHER_APPDROPPED    0x0026
    //// Cancelled: HWND = hwnd of app thumbnail, objectID = 0, childID = 0
    //#define EVENT_SYSTEM_SWITCHER_CANCELLED     0x0027
    //#endif /* _WIN32_WINNT >= 0x0602 */


    //#if(_WIN32_WINNT >= 0x0602)

    ///*
    // * Sent when an IME's soft key is pressed and should be echoed,
    // * but is not passed through the keyboard hook.
    // * Must not be sent when a key is sent through the keyboard hook.
    // *     HWND             is the hwnd of the UI containing the soft key
    // *     idChild          is the Unicode value of the character entered
    // *     idObject         is a bitfield
    // *         0x00000001: set if a 32-bit Unicode surrogate pair is used
    // */
    //#define EVENT_SYSTEM_IME_KEY_NOTIFICATION  0x0029

    //#endif /* _WIN32_WINNT >= 0x0602 */


    //#if(_WIN32_WINNT >= 0x0601)
    //#define EVENT_SYSTEM_END        0x00FF

    //#define EVENT_OEM_DEFINED_START     0x0101
    //#define EVENT_OEM_DEFINED_END       0x01FF

    //#define EVENT_UIA_EVENTID_START         0x4E00
    //#define EVENT_UIA_EVENTID_END           0x4EFF

    //#define EVENT_UIA_PROPID_START          0x7500
    //#define EVENT_UIA_PROPID_END            0x75FF
    //#endif /* _WIN32_WINNT >= 0x0601 */

    //#if(_WIN32_WINNT >= 0x0501)
    //#define EVENT_CONSOLE_CARET             0x4001
    //#define EVENT_CONSOLE_UPDATE_REGION     0x4002
    //#define EVENT_CONSOLE_UPDATE_SIMPLE     0x4003
    //#define EVENT_CONSOLE_UPDATE_SCROLL     0x4004
    //#define EVENT_CONSOLE_LAYOUT            0x4005
    //#define EVENT_CONSOLE_START_APPLICATION 0x4006
    //#define EVENT_CONSOLE_END_APPLICATION   0x4007

    ///*
    // * Flags for EVENT_CONSOLE_START/END_APPLICATION.
    // */
    //#if defined(_WIN64)
    //#define CONSOLE_APPLICATION_16BIT       0x0000
    //#else
    //#define CONSOLE_APPLICATION_16BIT       0x0001
    //#endif

    ///*
    // * Flags for EVENT_CONSOLE_CARET
    // */
    //#define CONSOLE_CARET_SELECTION         0x0001
    //#define CONSOLE_CARET_VISIBLE           0x0002
    //#endif /* _WIN32_WINNT >= 0x0501 */

    //#if(_WIN32_WINNT >= 0x0601)
    //#define EVENT_CONSOLE_END       0x40FF
    //#endif /* _WIN32_WINNT >= 0x0601 */

    ///*
    // * Object events
    // *
    // * The system AND apps generate these.  The system generates these for
    // * real windows.  Apps generate these for objects within their window which
    // * act like a separate control, e.g. an item in a list view.
    // *
    // * When the system generate them, dwParam2 is always WMOBJID_SELF.  When
    // * apps generate them, apps put the has-meaning-to-the-app-only ID value
    // * in dwParam2.
    // * For all events, if you want detailed accessibility information, callers
    // * should
    // *      * Call AccessibleObjectFromWindow() with the hwnd, idObject parameters
    // *          of the event, and IID_IAccessible as the REFIID, to get back an
    // *          IAccessible* to talk to
    // *      * Initialize and fill in a VARIANT as VT_I4 with lVal the idChild
    // *          parameter of the event.
    // *      * If idChild isn't zero, call get_accChild() in the container to see
    // *          if the child is an object in its own right.  If so, you will get
    // *          back an IDispatch* object for the child.  You should release the
    // *          parent, and call QueryInterface() on the child object to get its
    // *          IAccessible*.  Then you talk directly to the child.  Otherwise,
    // *          if get_accChild() returns you nothing, you should continue to
    // *          use the child VARIANT.  You will ask the container for the properties
    // *          of the child identified by the VARIANT.  In other words, the
    // *          child in this case is accessible but not a full-blown object.
    // *          Like a button on a titlebar which is 'small' and has no children.
    // */

    ///*
    // * For all EVENT_OBJECT events,
    // *      hwnd is the dude to Send the WM_GETOBJECT message to (unless NULL,
    // *          see above for system things)
    // *      idObject is the ID of the object that can resolve any queries a
    // *          client might have.  It's a way to deal with windowless controls,
    // *          controls that are just drawn on the screen in some larger parent
    // *          window (like SDM), or standard frame elements of a window.
    // *      idChild is the piece inside of the object that is affected.  This
    // *          allows clients to access things that are too small to have full
    // *          blown objects in their own right.  Like the thumb of a scrollbar.
    // *          The hwnd/idObject pair gets you to the container, the dude you
    // *          probably want to talk to most of the time anyway.  The idChild
    // *          can then be passed into the acc properties to get the name/value
    // *          of it as needed.
    // *
    // * Example #1:
    // *      System propagating a listbox selection change
    // *      EVENT_OBJECT_SELECTION
    // *          hwnd == listbox hwnd
    // *          idObject == OBJID_WINDOW
    // *          idChild == new selected item, or CHILDID_SELF if
    // *              nothing now selected within container.
    // *      Word '97 propagating a listbox selection change
    // *          hwnd == SDM window
    // *          idObject == SDM ID to get at listbox 'control'
    // *          idChild == new selected item, or CHILDID_SELF if
    // *              nothing
    // *
    // * Example #2:
    // *      System propagating a menu item selection on the menu bar
    // *      EVENT_OBJECT_SELECTION
    // *          hwnd == top level window
    // *          idObject == OBJID_MENU
    // *          idChild == ID of child menu bar item selected
    // *
    // * Example #3:
    // *      System propagating a dropdown coming off of said menu bar item
    // *      EVENT_OBJECT_CREATE
    // *          hwnd == popup item
    // *          idObject == OBJID_WINDOW
    // *          idChild == CHILDID_SELF
    // *
    // * Example #4:
    // *
    // * For EVENT_OBJECT_REORDER, the object referred to by hwnd/idObject is the
    // * PARENT container in which the zorder is occurring.  This is because if
    // * one child is zordering, all of them are changing their relative zorder.
    // */
    //#define EVENT_OBJECT_CREATE                 0x8000  // hwnd + ID + idChild is created item
    //#define EVENT_OBJECT_DESTROY                0x8001  // hwnd + ID + idChild is destroyed item
    //#define EVENT_OBJECT_SHOW                   0x8002  // hwnd + ID + idChild is shown item
    //#define EVENT_OBJECT_HIDE                   0x8003  // hwnd + ID + idChild is hidden item
    //#define EVENT_OBJECT_REORDER                0x8004  // hwnd + ID + idChild is parent of zordering children
    ///*
    // * NOTE:
    // * Minimize the number of notifications!
    // *
    // * When you are hiding a parent object, obviously all child objects are no
    // * longer visible on screen.  They still have the same "visible" status,
    // * but are not truly visible.  Hence do not send HIDE notifications for the
    // * children also.  One implies all.  The same goes for SHOW.
    // */


    //#define EVENT_OBJECT_FOCUS                  0x8005  // hwnd + ID + idChild is focused item
    //#define EVENT_OBJECT_SELECTION              0x8006  // hwnd + ID + idChild is selected item (if only one), or idChild is OBJID_WINDOW if complex
    //#define EVENT_OBJECT_SELECTIONADD           0x8007  // hwnd + ID + idChild is item added
    //#define EVENT_OBJECT_SELECTIONREMOVE        0x8008  // hwnd + ID + idChild is item removed
    //#define EVENT_OBJECT_SELECTIONWITHIN        0x8009  // hwnd + ID + idChild is parent of changed selected items

    ///*
    // * NOTES:
    // * There is only one "focused" child item in a parent.  This is the place
    // * keystrokes are going at a given moment.  Hence only send a notification
    // * about where the NEW focus is going.  A NEW item getting the focus already
    // * implies that the OLD item is losing it.
    // *
    // * SELECTION however can be multiple.  Hence the different SELECTION
    // * notifications.  Here's when to use each:
    // *
    // * (1) Send a SELECTION notification in the simple single selection
    // *     case (like the focus) when the item with the selection is
    // *     merely moving to a different item within a container.  hwnd + ID
    // *     is the container control, idChildItem is the new child with the
    // *     selection.
    // *
    // * (2) Send a SELECTIONADD notification when a new item has simply been added
    // *     to the selection within a container.  This is appropriate when the
    // *     number of newly selected items is very small.  hwnd + ID is the
    // *     container control, idChildItem is the new child added to the selection.
    // *
    // * (3) Send a SELECTIONREMOVE notification when a new item has simply been
    // *     removed from the selection within a container.  This is appropriate
    // *     when the number of newly selected items is very small, just like
    // *     SELECTIONADD.  hwnd + ID is the container control, idChildItem is the
    // *     new child removed from the selection.
    // *
    // * (4) Send a SELECTIONWITHIN notification when the selected items within a
    // *     control have changed substantially.  Rather than propagate a large
    // *     number of changes to reflect removal for some items, addition of
    // *     others, just tell somebody who cares that a lot happened.  It will
    // *     be faster an easier for somebody watching to just turn around and
    // *     query the container control what the new bunch of selected items
    // *     are.
    // */

    //#define EVENT_OBJECT_STATECHANGE            0x800A  // hwnd + ID + idChild is item w/ state change
    ///*
    // * Examples of when to send an EVENT_OBJECT_STATECHANGE include
    // *      * It is being enabled/disabled (USER does for windows)
    // *      * It is being pressed/released (USER does for buttons)
    // *      * It is being checked/unchecked (USER does for radio/check buttons)
    // */
    //#define EVENT_OBJECT_LOCATIONCHANGE         0x800B  // hwnd + ID + idChild is moved/sized item

    ///*
    // * Note:
    // * A LOCATIONCHANGE is not sent for every child object when the parent
    // * changes shape/moves.  Send one notification for the topmost object
    // * that is changing.  For example, if the user resizes a top level window,
    // * USER will generate a LOCATIONCHANGE for it, but not for the menu bar,
    // * title bar, scrollbars, etc.  that are also changing shape/moving.
    // *
    // * In other words, it only generates LOCATIONCHANGE notifications for
    // * real windows that are moving/sizing.  It will not generate a LOCATIONCHANGE
    // * for every non-floating child window when the parent moves (the children are
    // * logically moving also on screen, but not relative to the parent).
    // *
    // * Now, if the app itself resizes child windows as a result of being
    // * sized, USER will generate LOCATIONCHANGEs for those dudes also because
    // * it doesn't know better.
    // *
    // * Note also that USER will generate LOCATIONCHANGE notifications for two
    // * non-window sys objects:
    // *      (1) System caret
    // *      (2) Cursor
    // */

    //#define EVENT_OBJECT_NAMECHANGE             0x800C  // hwnd + ID + idChild is item w/ name change
    //#define EVENT_OBJECT_DESCRIPTIONCHANGE      0x800D  // hwnd + ID + idChild is item w/ desc change
    //#define EVENT_OBJECT_VALUECHANGE            0x800E  // hwnd + ID + idChild is item w/ value change
    //#define EVENT_OBJECT_PARENTCHANGE           0x800F  // hwnd + ID + idChild is item w/ new parent
    //#define EVENT_OBJECT_HELPCHANGE             0x8010  // hwnd + ID + idChild is item w/ help change
    //#define EVENT_OBJECT_DEFACTIONCHANGE        0x8011  // hwnd + ID + idChild is item w/ def action change
    //#define EVENT_OBJECT_ACCELERATORCHANGE      0x8012  // hwnd + ID + idChild is item w/ keybd accel change

    //#if(_WIN32_WINNT >= 0x0600)
    //#define EVENT_OBJECT_INVOKED                0x8013  // hwnd + ID + idChild is item invoked
    //#define EVENT_OBJECT_TEXTSELECTIONCHANGED   0x8014  // hwnd + ID + idChild is item w? test selection change

    ///*
    // * EVENT_OBJECT_CONTENTSCROLLED
    // * Sent when ending the scrolling of a window object.
    // *
    // * Unlike the similar event (EVENT_SYSTEM_SCROLLEND), this event will be
    // * associated with the scrolling window itself. There is no difference
    // * between horizontal or vertical scrolling.
    // *
    // * This event should be posted whenever scroll action is completed, including
    // * when it is scrolled by scroll bars, mouse wheel, or keyboard navigations.
    // *
    // *   example:
    // *          hwnd == window that is scrolling
    // *          idObject == OBJID_CLIENT
    // *          idChild == CHILDID_SELF
    // */
    //#define EVENT_OBJECT_CONTENTSCROLLED        0x8015
    //#endif /* _WIN32_WINNT >= 0x0600 */

    //#if(_WIN32_WINNT >= 0x0601)
    //#define EVENT_SYSTEM_ARRANGMENTPREVIEW      0x8016
    //#endif /* _WIN32_WINNT >= 0x0601 */

    //#if(_WIN32_WINNT >= 0x0602)

    ///*
    // * EVENT_OBJECT_CLOAKED / UNCLOAKED
    // * Sent when a window is cloaked or uncloaked.
    // * A cloaked window still exists, but is invisible to
    // * the user.
    // */
    //#define EVENT_OBJECT_CLOAKED                0x8017
    //#define EVENT_OBJECT_UNCLOAKED              0x8018

    ///*
    // * EVENT_OBJECT_LIVEREGIONCHANGED
    // * Sent when an object that is part of a live region
    // * changes.  A live region is an area of an application
    // * that changes frequently and/or asynchronously, so
    // * that an assistive technology tool might want to pay
    // * special attention to it.
    // */
    //#define EVENT_OBJECT_LIVEREGIONCHANGED      0x8019

    ///*
    // * EVENT_OBJECT_HOSTEDOBJECTSINVALIDATED
    // * Sent when a window that is hosting other Accessible
    // * objects changes the hosted objects.  A client may
    // * wish to requery to see what the new hosted objects are,
    // * especially if it has been monitoring events from this
    // * window.  A hosted object is one with a different Accessibility
    // * framework (MSAA or UI Automation) from its host.
    // *
    // * Changes in hosted objects with the *same* framework
    // * as the parent should be handed with the usual structural
    // * change events, such as EVENT_OBJECT_CREATED for MSAA.
    // * see above.
    // */
    //#define EVENT_OBJECT_HOSTEDOBJECTSINVALIDATED 0x8020

    ///*
    // * Drag / Drop Events
    // * These events are used in conjunction with the
    // * UI Automation Drag/Drop patterns.
    // *
    // * For DRAGSTART, DRAGCANCEL, and DRAGCOMPLETE,
    // * HWND+objectID+childID refers to the object being dragged.
    // *
    // * For DRAGENTER, DRAGLEAVE, and DRAGDROPPED,
    // * HWND+objectID+childID refers to the target of the drop
    // * that is being hovered over.
    // */

    //#define EVENT_OBJECT_DRAGSTART              0x8021
    //#define EVENT_OBJECT_DRAGCANCEL             0x8022
    //#define EVENT_OBJECT_DRAGCOMPLETE           0x8023

    //#define EVENT_OBJECT_DRAGENTER              0x8024
    //#define EVENT_OBJECT_DRAGLEAVE              0x8025
    //#define EVENT_OBJECT_DRAGDROPPED            0x8026

    ///*
    // * EVENT_OBJECT_IME_SHOW/HIDE
    // * Sent by an IME window when it has become visible or invisible.
    // */
    //#define EVENT_OBJECT_IME_SHOW               0x8027
    //#define EVENT_OBJECT_IME_HIDE               0x8028

    ///*
    // * EVENT_OBJECT_IME_CHANGE
    // * Sent by an IME window whenever it changes size or position.
    // */
    //#define EVENT_OBJECT_IME_CHANGE             0x8029

    //#endif /* _WIN32_WINNT >= 0x0602 */

    //#if(_WIN32_WINNT >= 0x0601)
    //#define EVENT_OBJECT_END                    0x80FF

    //#define EVENT_AIA_START                     0xA000
    //#define EVENT_AIA_END                       0xAFFF
    //#endif /* _WIN32_WINNT >= 0x0601 */
    #endregion

    public enum EventHookType : uint
    {
        #region ~ Min/Max ~
        Min = 0x00000001,
        Max = 0x7FFFFFFF,
        #endregion

        #region ~ System events ~
        System_Sound = 0x0001,
        System_Alert,
        System_Foreground,
        System_MenuStart,
        System_MenuEnd,
        System_MenuPopupStart,
        System_MenuPopupEnd,
        System_CaptureStart,
        System_CaptureEnd,
        System_MoveSizeStart,
        System_MoveSizeEnd,
        System_ContextHelpStart,
        System_ContextHelpEnd,
        System_DragDropStart,
        System_DragDropEnd,
        System_DialogStart,
        System_DialogEnd,
        System_ScrollingStart,
        System_ScrollingEnd,
        System_SwitchStart,
        System_SwitchEnd,
        System_MinimizeStart,
        System_MinimizeEnd,
        /// <summary>
        /// WinVersion >= 0x0600
        /// </summary>
        System_DesktopSwitch = 0x0020,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        System_Switcher_AppGrabbed = 0x0024,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        System_Switcher_AppOverTarget,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        System_Switcher_AppDropped,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        System_Switcher_Cancelled,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        System_ImeKey_Notification = 0x0029,
        /// <summary>
        /// WinVersion >= 0x0601
        /// </summary>
        System_End = 0x00FF,
        #endregion

        #region ~ OEM events ~
        /// <summary>
        /// WinVersion >= 0x0601
        /// </summary>
        Oem_Start = 0x0101,
        /// <summary>
        /// WinVersion >= 0x0601
        /// </summary>
        Oem_End = 0x01FF,
        #endregion

        #region ~ UI Automation events ~
        /// <summary>
        /// WinVersion >= 0x0601
        /// </summary>
        Uia_EventId_Start = 0x4E00,
        /// <summary>
        /// WinVersion >= 0x0601
        /// </summary>
        Uia_EventId_End = 0x4EFF,
        #endregion

        #region ~ UI Automation property-changed events ~
        /// <summary>
        /// WinVersion >= 0x0601
        /// </summary>
        Uia_PropId_Start = 0x7500,
        /// <summary>
        /// WinVersion >= 0x0601
        /// </summary>
        Uia_PropId_End = 0x75FF,
        #endregion

        #region ~ Console events ~
        /// <summary>
        /// WinVersion >= 0x0501
        /// </summary>
        Console_Caret = 0x4001,
        /// <summary>
        /// WinVersion >= 0x0501
        /// </summary>
        Console_UpdateRegion,
        /// <summary>
        /// WinVersion >= 0x0501
        /// </summary>
        Console_UpdateSimple,
        /// <summary>
        /// WinVersion >= 0x0501
        /// </summary>
        Console_UpdateScroll,
        /// <summary>
        /// WinVersion >= 0x0501
        /// </summary>
        Console_Layout,
        /// <summary>
        /// WinVersion >= 0x0501
        /// </summary>
        Console_StartApplication,
        /// <summary>
        /// WinVersion >= 0x0501
        /// </summary>
        Console_EndApplication,
        /// <summary>
        /// WinVersion >= 0x0601
        /// </summary>
        Console_End = 0x40FF,
        #endregion

        #region ~ Object events ~
        Object_Create = 0x8000,
        Object_Destroy,
        Object_Show,
        Object_Hide,
        Object_Reorder,
        Object_Focus,
        Object_Selection,
        Object_SelectionAdd,
        Object_SelectionRemove,
        Object_SelectionWithin,
        Object_StateChange,
        Object_LocationChange,
        Object_NameChange,
        Object_DescriptionChange,
        Object_ValueChange,
        Object_ParentChange,
        Object_HelpChange,
        Object_DefActionChange,
        Object_AcceleratorChange,
        /// <summary>
        /// WinVersion >= 0x0600
        /// </summary>
        Object_Invoked,
        /// <summary>
        /// WinVersion >= 0x0600
        /// </summary>
        Object_TextSelectionChanged,
        /// <summary>
        /// WinVersion >= 0x0600
        /// </summary>
        Object_ContentScrolled,
        /// <summary>
        /// WinVersion >= 0x0601
        /// </summary>
        Object_System_ArrangmentPreview,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        Object_Cloaked,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        Object_Uncloked,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        Object_LiveRegionChanged,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        Object_HostedObjectsInvalidated = 0x8020,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        Object_DragStart,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        Object_DragCancel,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        Object_DragComplete,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        Object_DragEnter,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        Object_DragLeave,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        Object_DragDropped,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        Object_Ime_Show,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        Object_Ime_Hide,
        /// <summary>
        /// WinVersion >= 0x0602
        /// </summary>
        Object_Ime_Change,
        /// <summary>
        /// WinVersion >= 0x0601
        /// </summary>
        Object_End = 0x80FF,
        #endregion

        #region ~ Accessibility Interoperability Alliance (AIA) events ~
        /// <summary>
        /// WinVersion >= 0x0601
        /// </summary>
        Aia_Start = 0xA000,
        /// <summary>
        /// WinVersion >= 0x0601
        /// </summary>
        Aia_End = 0xAFFF,
        #endregion
    }
}