﻿using System;
using System.Runtime.InteropServices;

internal static partial class Interop
{
    const string User32Lib = "user32.dll";
    /// <summary>
    /// Events are ASYNC.
    /// </summary>
    internal const uint WINEVENT_OUTOFCONTEXT = 0x0000;
    /// <summary>
    /// Don't call back for events on installer's thread.
    /// </summary>
    internal const uint WINEVENT_SKIPOWNTHREAD = 0x0001;
    /// <summary>
    /// Don't call back for events on installer's process.
    /// </summary>
    internal const uint WINEVENT_SKIPOWNPROCESS = 0x0002;
    /// <summary>
    /// Events are SYNC, this causes your dll to be injected into every process.
    /// </summary>
    /// <remarks>
    /// Not used.
    /// </remarks>
    internal const uint WINEVENT_INCONTEXT = 0x0004;

    internal delegate void WINEVENTPROC (
        IntPtr hWinEventHook,
        uint dwEvent,
        IntPtr hWnd,
        int idObject,
        int idChild,
        uint dwEventThread,
        uint dwmsEventTime
        );

    [DllImport( User32Lib, SetLastError = true )]
    internal static extern IntPtr SetWinEventHook (
        [In] uint eventMin,
        [In] uint eventMax,
        [In] IntPtr hmodWinEventProc,
        [MarshalAs( UnmanagedType.FunctionPtr )]
            [In] WINEVENTPROC lpfnWinEventProc,
            [In] int idProcess,
            [In] int idThread,
            [In] uint dwflags
            );

    [DllImport( User32Lib, SetLastError = true )]
    [return: MarshalAs( UnmanagedType.Bool )]
    internal static extern bool UnhookWinEvent (
        [In]  IntPtr hWinEventHook
        );
}