﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace WindowsEventsHook
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// The client thread that calls SetWinEventHook must have a message loop in order to receive events.
    /// </remarks>
    public class WinEventsHook : IDisposable
    {
        [MarshalAs( UnmanagedType.FunctionPtr )]
        private Interop.WINEVENTPROC mWinEventProc;

        private IntPtr mWinEventHandle;
        private int mProcessId;
        private int mThreadId;
        private EventHookType mLowerEventHookType;
        private EventHookType mHigherEventHookType;

        /// <summary>
        /// Occurs when the event received.
        /// </summary>
        public event EventHandler<WinEventArgs> EventReceived;

        #region ~ Constructors ~

        /// <summary>
        /// Create instance of WinEventHook class with '<see cref="EventHookType.Min"/>' lower event value
        /// and '<see cref="EventHookType.Max"/>' higher event value, and receives events from all processes and threads.
        /// </summary>
        public WinEventsHook ( )
            : this( 0, 0, EventHookType.Min, EventHookType.Max )
        {
        }
        /// <summary>
        /// Create instance of WinEventHook class with setted lower and higher event values, 
        /// and receives events from all processes and threads.
        /// </summary>
        /// <param name="lowerEvent">Lower event type.</param>
        /// <param name="higherEvent">Higher event type.</param>
        public WinEventsHook ( EventHookType lowerEvent, EventHookType higherEvent )
            : this( 0, 0, lowerEvent, higherEvent )
        {
        }
        /// <summary>
        /// Create instance of WinEventHook class with setted lower and higher event values, 
        /// and receives events from specified process.
        /// </summary>
        /// <param name="processId"></param>
        /// <param name="lowerEvent"></param>
        /// <param name="higherEvent"></param>
        public WinEventsHook ( int processId, EventHookType lowerEvent, EventHookType higherEvent )
            : this( processId, 0, lowerEvent, higherEvent )
        {
        }
        /// <summary>
        /// Create instance of WinEventHook class with setted lower and higher event values,  
        /// and receives events from specified process and thread.
        /// </summary>
        /// <param name="processId"></param>
        /// <param name="threadId"></param>
        /// <param name="lowerEvent"></param>
        /// <param name="higherEvent"></param>
        public WinEventsHook ( int processId, int threadId, EventHookType lowerEvent, EventHookType higherEvent )
        {
            ProcessId = processId;
            ThreadId = threadId;
            HigherEventHookType = higherEvent;
            LowerEventHookType = lowerEvent;

            mWinEventProc = EventProc;
        }

        #endregion

        #region ~ Properties ~

        /// <summary>
        /// Specifies the ID of the process from which the hook function receives events.
        /// </summary>
        public int ProcessId
        {
            get { return mProcessId; }
            set
            {
                if ( mWinEventHandle != IntPtr.Zero )
                    throw new InvalidOperationException(
                        "Can't change process id: event setted!"
                        );
                if ( value < 0 )
                    throw new ArgumentException(
                        "Can't change process id: value can't be less than zero!"
                        );

                mProcessId = value;
            }
        }

        /// <summary>
        /// Specifies the ID of the thread from which the hook function receives events.
        /// </summary>
        public int ThreadId
        {
            get { return mThreadId; }
            set
            {
                if ( mWinEventHandle != IntPtr.Zero )
                    throw new InvalidOperationException(
                        "Can't change thread id: event setted!"
                        );
                if ( value < 0 )
                    throw new ArgumentException(
                        "Can't change thread id: value can't be less than zero!"
                        );

                mThreadId = value;
            }
        }

        /// <summary>
        /// Specifies the event for the lowest event value in the range of events that are handled by the hook function.
        /// </summary>
        public EventHookType LowerEventHookType
        {
            get { return mLowerEventHookType; }
            set
            {
                CheckEventType( value );

                if ( mWinEventHandle != IntPtr.Zero )
                    throw new InvalidOperationException(
                        "Can't change min event type: event setted!"
                        );
                if ( value > mHigherEventHookType )
                    throw new ArgumentException(
                        "Can't change min event type: value can't be more than max event type value!"
                        );

                mLowerEventHookType = value;
            }
        }

        /// <summary>
        /// Specifies the event for the highest event value in the range of events that are handled by the hook function.
        /// </summary>
        public EventHookType HigherEventHookType
        {
            get { return mLowerEventHookType; }
            set
            {
                CheckEventType( value );

                if ( mWinEventHandle != IntPtr.Zero )
                    throw new InvalidOperationException(
                        "Can't change max event type: event setted!"
                        );
                if ( value < mLowerEventHookType )
                    throw new ArgumentException(
                        "Can't change max event type: value can't be less than lower event type value!"
                        );

                mHigherEventHookType = value;
            }
        }

        /// <summary>
        /// Specifies when hook set.
        /// </summary>
        public bool IsInstalled
        {
            get { return mWinEventHandle != IntPtr.Zero; }
        }
        /// <summary>
        /// Prevents this instance of the hook from receiving the events that are generated by threads in this process.
        /// </summary>
        /// <remarks>
        /// This flag does not prevent threads from generating events.
        /// </remarks>
        public bool SkipOwnProcess { get; set; }
        /// <summary>
        /// Prevents this instance of the hook from receiving the events that are generated by the thread that is registering this hook.
        /// </summary>
        public bool SkipOwnThread { get; set; }

        #endregion

        #region ~ Methods ~

        /// <summary>
        /// Set hook.
        /// </summary>
        public void Set ( )
        {
            Set( LowerEventHookType, HigherEventHookType );
        }

        /// <summary>
        /// Set specified hook.
        /// </summary>
        /// <param name="lowerEvent"></param>
        /// <param name="higherEvent"></param>
        public void Set ( EventHookType lowerEvent, EventHookType higherEvent )
        {
            if ( mWinEventProc == null )
                throw new ObjectDisposedException( GetType().FullName );
            if ( mWinEventHandle != IntPtr.Zero )
                throw new InvalidOperationException( "Can't set event hook when another already set." );

            HigherEventHookType = higherEvent;
            LowerEventHookType = lowerEvent;

            var flags = Interop.WINEVENT_OUTOFCONTEXT;

            if ( SkipOwnProcess )
                flags |= Interop.WINEVENT_SKIPOWNPROCESS;
            if ( SkipOwnThread )
                flags |= Interop.WINEVENT_SKIPOWNTHREAD;

            mWinEventHandle = Interop.SetWinEventHook(
                (uint)lowerEvent, (uint)higherEvent, IntPtr.Zero,
                mWinEventProc, mProcessId, mThreadId, flags
                );

            if ( mWinEventHandle == IntPtr.Zero )
                throw new Win32Exception( Marshal.GetLastWin32Error() );
        }

        /// <summary>
        /// Remove hook.
        /// </summary>
        public void Remove ( )
        {
            if ( mWinEventProc == null )
                throw new ObjectDisposedException( GetType().FullName );
            if ( mWinEventHandle == IntPtr.Zero )
                throw new InvalidOperationException( "Can't stop event hook: hook not set!" );

            if ( !Interop.UnhookWinEvent( mWinEventHandle ) )
                throw new Win32Exception( Marshal.GetLastWin32Error() );

            mWinEventHandle = IntPtr.Zero;
        }

        /// <summary>
        /// Release resources and remove hook, if it already installed.
        /// </summary>
        public void Dispose ( )
        {
            if ( mWinEventProc != null )
            {
                if ( IsInstalled )
                    Remove();
                mWinEventProc = null;
            }
        }

        private void EventProc (
            IntPtr hWinEventHook,
            uint dwEvent,
            IntPtr hWnd,
            int idObject,
            int idChild,
            uint dwEventThread,
            uint dwmsEventTime
            )
        {
            var args = new WinEventArgs() {
                EventType = (EventHookType)dwEvent,
                WindowHandle = hWnd,
                EventThreadId = dwEventThread,
                EventTime = dwmsEventTime,
                ObjectId = idObject,
                ChildId = idChild
            };

            OnEventReceived( this, args );
        }

        private void OnEventReceived ( WinEventsHook owner, WinEventArgs e )
        {
            var handler = EventReceived;
            if ( handler != null )
                handler( owner, e );
        }

        private void CheckEventType ( EventHookType eventType )
        {
            if ( eventType < EventHookType.Min || eventType > EventHookType.Max )
                throw new ArgumentOutOfRangeException( "eventType" );
        }

        #endregion
    }
}