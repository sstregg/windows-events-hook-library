﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Windows;
using WindowsEventsHook;
using Example.Helpers;

namespace Example.ViewModels
{
    public class WinEventViewModel : INotifyPropertyChanged
    {
        private readonly WinEventsHook mWinEventHook;

        public WinEventViewModel ( )
        {
            Application.Current.Exit += ( sender, e ) => { if ( mWinEventHook.IsInstalled ) mWinEventHook.Remove(); };

            Events = new ObservableCollection<WinEventArgs>();
            mWinEventHook = new WinEventsHook( EventHookType.Object_Create, EventHookType.Object_Create );
            mWinEventHook.EventReceived += ( sender, e ) => Events.Add( e );

            InitCommands();
        }

        private void InitCommands ( )
        {
            SetHookCommand = new DelegateCommand( o => {
                Events.Clear();
                mWinEventHook.Set();
                SetHookCommand.RaiseCanExecuteChanged();
                RemoveHookCommand.RaiseCanExecuteChanged();
            }, o => !mWinEventHook.IsInstalled );
            RemoveHookCommand = new DelegateCommand( o => {
                mWinEventHook.Remove();
                SetHookCommand.RaiseCanExecuteChanged();
                RemoveHookCommand.RaiseCanExecuteChanged();
            }, o => mWinEventHook.IsInstalled );
        }

        #region ~ Properties ~

        public ObservableCollection<WinEventArgs> Events { get; private set; }

        #endregion


        #region ~ Commands ~

        public DelegateCommand SetHookCommand { get; private set; }
        public DelegateCommand RemoveHookCommand { get; private set; }

        #endregion


        #region ~ INotifyPropertyChanged ~

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged ( [CallerMemberName] string propertyName = null )
        {
            var handler = PropertyChanged;
            if ( handler != null )
                handler( this, new PropertyChangedEventArgs( propertyName ) );
        }

        protected void OnPropertyChanged<TProperty> ( Expression<Func<TProperty>> propertyExpression )
        {
            OnPropertyChanged( ((MemberExpression)propertyExpression.Body).Member.Name );
        }

        #endregion

    }
}
